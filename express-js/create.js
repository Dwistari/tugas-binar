const { books } = require('./models')

books.create({
    isbn: '004-623-333-29-4',
    judul: 'Romeo and Juliet ',
    sinopsi: 'Romeo and Juliet begitu terkenal hingga banyak dipentaskan dalam berbagai seni drama, film, dan musikal.',
    penulis: 'William Shakespeare',
    genre: 'Fiksi'
    })
    .then(book => {
    console.log(book)
    })
