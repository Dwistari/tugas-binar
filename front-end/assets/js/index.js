$(function(){
  navbarSmooth()
  progressBar()
  textTyping()
  dataProfile()
  activeNavbar()
})

// Fungsi digunakan untuk membuat scroll  section pada navbar secara smooth
function navbarSmooth(){
    $('.nav-link, .btn-about, .copyright').on("click",function(){
    
        var targetHref   = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(targetHref).offset().top  
        }, 1000);
            
    })
}

//Fungsi untuk memberi tanda pada current active navbar
function activeNavbar(){
    $( '#myNavbar .ml-auto a' ).on('click', 
    function () {
        $( '#myNavbar .ml-auto' ).find( 'li.active' )
        .removeClass( 'active' );
        $( this ).parent( 'li' ).addClass( 'active' );
    });
}

// Fungsi untuk memberikan animasi pada progressbar
function progressBar(){

    var $els = $('.progress-bar-wrap'); 
    var $window = $(window);
    
    $window.on('scroll', function(){
      $els.each(function(){ // Untuk menjalankan fungsi pada seluruh class progress-bar-wrap
        var $this = $(this); 
        if($window.scrollTop() > $this.offset().top - $window.height()){ // Mengecek halaman sudah terbuka
            $this.find('.progress-bar').css({'width' : $this.attr('data-percent')}); // Update tampilan presentase progresbar
            }
      });
    
    });
}

// Fungsi untuk memberikan efek typing pada text 
function textTyping(){
    var text = "Programmer";
    var chars = text.split('');
    var container = document.getElementsByClassName("lead");
    
    var i = 0;
    setInterval(function () {
        if (i < chars.length) {
            container[0].innerHTML += chars[i++];
        } else {
            i = 0;
            container[0].innerHTML = "I'm ";
        }
    }, 400);
    
}

// Fungsi memberikan data pada class data
function dataProfile(){
    let data = {
        dob : "24 Des 19966",
        phone : "+62 8570 6028 656",
        City: "Yogyakarta",
        email : "Dwistari16@gmail.com",
        degre :"Bachelor",
        website : "www.tari.com",
    }

    var element = document.getElementsByClassName("data-profile")
    element[0].innerHTML = "Birthday :" +" "+ data.dob;
    element[1].innerHTML = "Phone :" +" "+ data.phone;
    element[2].innerHTML = "City :" +" "+  data.City;
    element[3].innerHTML = "Email :" +" "+  data.email;
    element[4].innerHTML = "Degre :" +" "+  data.degre;
    element[5].innerHTML = "Website :" +" "+  data.website;

}

