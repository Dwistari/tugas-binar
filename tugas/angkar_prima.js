
/**
 * 
 * Diberikan sebuah function angkaPrima(angka) yang menerima satu parameter berupa angka.
 * Function akan me-return true jika angka tersebut adalah bilangan prima. Jika tidak, return false
 * 
 */


 function angkaPrima(angka) {
     var pembagi = 0  // variable yg digunakan untuk menyimpan berapa banyak angka itu bisa dibagi
    for(let i =1; i <=  angka; i++){
        if(angka %i == 0){
            pembagi++
        }
    }
    if(pembagi == 2){   // jika angka hanya bisa dibagi 1 atau bilangan itu sendiri yg berarti 2x pembagian , yaitu true
        return true
      }else{
        return false
      }
}

// TEST CASES
console.log(angkaPrima(3)); // true
console.log(angkaPrima(7)); // true
console.log(angkaPrima(6)); // false
console.log(angkaPrima(23)); // true
console.log(angkaPrima(33)); // false
