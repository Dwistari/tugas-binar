
/**
 * 
 * Diberikan sebuah function changeMe(arr) yang menerima satu parameter
 * berupa array multidimensi dimana array tersebut berisi value (pasti berurut dari kiri ke kanan)
 * First Name, Last Name, Gender dan Birth Year.
 * 
 * Fungsi ini akan menampilkan object literal yang memiliki property firstName, lastName, gender dan age.
 * Nilai age didapatkan dari tahun sekarang dikurang tahun lahir.
 * Jika tahun lahir tidak diisi atau tahun lahir lebih besar dibandingkan tahun sekarang maka age akan berisi 'Invalid Birth Year'
 * 
 * Contoh jika arr inputan adalah [['Platinum', 'Fox', 'female', 1995], ['John', 'Doe', 'male', 2000]] maka output:
 * 
 * Platinum Fox: { firstName: 'Platinum', lastName: 'Fox', gender: 'female', age: 23 }
 * John Doe: { firstName: 'John', lastName: 'Doe', gender: 'male', age: 18 }
 * 
 */


function changeMe(arr) {
   
  var output = []  // untuk menyimpan output array
  var currentYear = new Date().getFullYear(); // Untuk mendapatkan tahun berapa saat ini

  for ( var i = 0; i < arr.length; i+=1 ) {
      var item = arr[i];
      output.push({
          firstName : item[0],
          lastName : item[1],
          gender: item[2],
          age : item[3] === undefined ? "Invalid Birth Year" : currentYear - item[3]
      })
  }

  if (output.length != 0){
    output.forEach((o, index )=> { 
      console.log(index + 1 + ". "+o.firstName+" "+o.lastName+": "+JSON.stringify(o));
    })
  }else{
    console.log("")
  }
}

// TEST CASES
changeMe([['Christ', 'Evans', 'Male', 1982], ['Robert', 'Downey', 'Male']]);
// 1. Christ Evans:
// { firstName: 'Christ',
//   lastName: 'Evans',
//   gender: 'Male',
//   age: 37 }
// 2. Robert Downey:
// { firstName: 'Robert',
//   lastName: 'Downey',
//   gender: 'Male',
//   age: 'Invalid Birth Year' }

changeMe([]); // ""
