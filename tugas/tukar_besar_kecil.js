
/**
 * 
 * Diberikan function tukarBesarKecil(kalimat) yang menerima satu parameter berupa string.
 * Function akan me-return string tersebut dengan menukar karakter yang uppercase menjadi lowercase, dan sebaliknya.
 * Spasi dan simbol diabaikan.
 * 
 */


 function tukarBesarKecil(kalimat) {
    const isUpperCase = char => char.charCodeAt(0) >= 65 && char.charCodeAt(0)<= 90; //Digunakan untuk mendefinisi abjad a-z dengan huruf uppercase
    const isLowerCase = char => char.charCodeAt(0) >= 97 &&char.charCodeAt(0) <= 122; //Digunakan untuk mendefinisi abjad a-z dengan huruf lowercase
       let newKalimat = '';
       const margin = 32; // Mendefinisikan spasi
      
       for(let i = 0; i < kalimat.length; i++){
          var kata = kalimat[i];
          if(isLowerCase(kata)){
             newKalimat += String.fromCharCode(kata.charCodeAt(0) - margin);
          }else if(isUpperCase(kata)){
             newKalimat += String.fromCharCode(kata.charCodeAt(0) + margin);
          }else{
             newKalimat += kata;
          };
       };
       return newKalimat;
    }
    
  
// TEST CASES
console.log(tukarBesarKecil('Hello World')); // "hELLO wORLD"
console.log(tukarBesarKecil('I aM aLAY')); // "i Am Alay"
console.log(tukarBesarKecil('My Name is Bond!!')); // "mY nAME IS bOND!!"
console.log(tukarBesarKecil('IT sHOULD bE me')); // "it Should Be ME"
console.log(tukarBesarKecil('001-A-3-5TrdYW')); // "001-a-3-5tRDyw"
